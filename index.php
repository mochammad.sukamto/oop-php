<?php

require_once('animal.php');
require_once('ape.php');
require_once('frog.php');

$sheep = new Animal("shaun");

echo "Name : $sheep->name <br>"; // "shaun"
echo "legs : $sheep->legs<br>"; // 4
echo "cold blooded : $sheep->cold_blooded<br><br>"; // "no"

$Sungokong = new Ape ("Kera");

echo "Name : $Sungokong->name <br>"; // "shaun"
echo "legs : $Sungokong->legs<br>"; // 4
echo "cold blooded : $Sungokong->cold_blooded<br>"; // "no"
$Sungokong->yell();
echo "<br><br>";

$kodok = new Frog ("Buduk");

echo "Name : $kodok->name <br>"; // "shaun"
echo "legs : $kodok->legs<br>"; // 4
echo "cold blooded : $kodok->cold_blooded<br>"; // "no"
$kodok->jump();
echo "<br><br>";